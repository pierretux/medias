# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- #4958 Appel à la globale `$formats_logos` remplacée par `_image_extensions_acceptees_en_entree()`
- Externalisation de la lib [james-heinrich/getid3](https://packagist.org/packages/james-heinrich/getid3)
- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev
